package monProjet.stepdefinition;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.pageobject.ConnexionPage;
import org.pageobject.HomePage;
import java.time.Duration;
import static org.junit.jupiter.api.Assertions.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class StepDefinitions {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Actions actions;
    protected String browser = "chrome";
    protected Boolean maximizeDriver = true;
    protected Integer implicitWaitingTime = 5;
    protected Integer explicitWaitingTime = 10;
    protected String url = "https://petstore.octoperf.com/actions/Account.action;jsessionid=E310F0AE0CFB2D6BC8962BC796F0B14D?signonForm=";

    @BeforeAll
    public static void setupAll() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setup() {
        switch (browser.toLowerCase()) {
            case "firefox" :
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.addArguments();
                driver = new FirefoxDriver(firefoxOptions);
                break;
            case "chrome" :
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--remote-allow-origins=*");
                driver = new ChromeDriver(chromeOptions);
                break;
        }

        // initiate driver and wait
        if (maximizeDriver) {
            driver.manage().window().maximize();
        }
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(implicitWaitingTime));
        wait = new WebDriverWait(driver, Duration.ofSeconds(explicitWaitingTime));
        actions = new Actions(driver);
        driver.get(url);
    }

    @After
    public void teardown() {
        driver.quit();
    }

    @Given("je suis sur la page d'authentification$")
    // function to go to authentification page
    public void je_suis_sur_page_authentification() {
        ConnexionPage Connexion = new ConnexionPage(driver);
        assertTrue(Connexion.connexionAddText.isDisplayed(), "We are not on the authentification page");
    }

    @When("je saisis le username {string} et le mot de passe {string}")
    // function to login to website
    public void jeSaisisLeUsernameUsrEtLeMotDePassePwd(String username, String password) {
        ConnexionPage Connexion = new ConnexionPage(driver);
        Connexion.connexionLoginInput.clear();
        Connexion.connexionLoginInput.sendKeys(username);
        Connexion.connexionPwdInput.clear();
        Connexion.connexionPwdInput.sendKeys(password);
    }

    @And("je clique sur le bouton connexion")
    public void jeCliqueSurLeBoutonConnexion() {
        ConnexionPage Connexion = new ConnexionPage(driver);
        Connexion.connexionLoginButton.click();
    }

    @Then("je suis authentifié sur le site")
    public void jeSuisAuthentifiéSurLeSite() {
        HomePage Home = new HomePage(driver);
        assertTrue(Home.homeMyAccountButton.isDisplayed());
    }

    @And("le message {string} apparaît")
    public void leMessageMessageApparaît(String welcomeMessage) {
        HomePage Home = new HomePage(driver);
        assertEquals(welcomeMessage, Home.homeWelcomeText.getText(), "Le message d'accueil n'est pas bon");
    }
}
