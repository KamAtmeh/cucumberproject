# language: en
Feature: Connexion JPetStore

	Scenario Outline: Connexion JPetStore
		Given je suis sur la page d'authentification
		When je saisis le username <usr> et le mot de passe <pwd>
		And je clique sur le bouton connexion
		Then je suis authentifié sur le site
		And le message <message> apparaît

		@Passant
		Examples:
		| message | pwd | usr |
		| "Welcome ABC!" | "j2ee" | "j2ee" |