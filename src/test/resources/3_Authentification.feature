# language: en
Feature: Authentification

  Background: L'utilisateur a accès au site
    Given J'ai accès au site
    
  Scenario Outline: S'authentifier sur le site
    Given je me trouve sur la page d'authentification du site
    When je dispose d'un compte utilisateur <usr>
    And je saisis l'identifiant <usr> dans le champ "Login"
    And je saisis le mot de passe <pwd> dans le champ "Password"
    And je clique sur le bouton "Connexion" du formulaire
    Then <succesAuthentification>
    And <affichage>
    
    Examples:
      | usr | pwd | succesAuthentification  | affichage |
      | Marie-Hélène  | secret  | je suis authentifié sur le site  | je suis redirigé vers la page "Mon Compte" et je peux accéder à mes données personnelles |
      | toto  |   | je ne suis pas authentifié sur le site | le message d'erreur "mot de passe" invalide apparaît  |
      |   | toto  | je ne suis pas authentifié sur le site  | le message d'erreur "nom d'utilisateur" invalide apparaît |