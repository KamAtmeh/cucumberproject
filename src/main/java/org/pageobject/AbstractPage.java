package org.pageobject;

import org.openqa.selenium.WebDriver;

public class AbstractPage{

    // ***** Variables ***** //
    protected final WebDriver driver;

    // ***** Constructeur ***** //
    public AbstractPage (WebDriver driver){
        this.driver = driver;
    }

}