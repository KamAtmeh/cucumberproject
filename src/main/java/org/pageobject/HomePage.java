package org.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends AbstractPage{

    // ********* Constructor ******** //
    public HomePage (WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    // ********** Web Elements *********** //

    //Sign In Button
    @FindBy(linkText = "My Account")
    public WebElement homeMyAccountButton;

    @FindBy(id = "WelcomeContent")
    public WebElement homeWelcomeText;

}
