package org.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConnexionPage extends AbstractPage{

    // ********* Constructor ******** //
    public ConnexionPage (WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    // ********** Web Elements *********** //

    //Text to add username and password
    @FindBy(xpath = "//p[text()=\"Please enter your username and password.\"]")
    public WebElement connexionAddText;

    //Login Input
    @FindBy(name = "username")
    public WebElement connexionLoginInput;

    //Password Input
    @FindBy(name = "password")
    public WebElement connexionPwdInput;

    //Login Button
    @FindBy(name = "signon")
    public WebElement connexionLoginButton;

}
